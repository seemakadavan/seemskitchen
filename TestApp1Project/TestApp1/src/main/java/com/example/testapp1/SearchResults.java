package com.example.testapp1;

import android.app.Activity;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.SearchView;
import android.widget.Toast;

/**
 * Created by skadavan on 6/18/13.
 */
public class SearchResults extends Activity {

    // TODO: How to initialise to empty string
    String query = "";

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        // Get the intent, verify the action and get the query
        Intent intent = getIntent();
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            query = intent.getStringExtra(SearchManager.QUERY);
            doMySearch(query);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        SetUpSearchView(menu);
        return true;
    }

    private void doMySearch(String searchQry)
    {
        MakeToast(searchQry);
    }

    private void SetUpSearchView(Menu menu)
    {
        MenuItem searchMenuItem = menu.findItem( R.id.search );
        if(searchMenuItem == null)
        {
            return;
        }
        /*
        int sdk = android.os.Build.VERSION.SDK_INT;
        if(sdk > android.os.Build.VERSION_CODES.HONEYCOMB)
        {
            // pre honeycomb
        }
        else
        {
            // honeycomb and post
        }
        */
        // TODO: expandActionView() works only on API 14+
        searchMenuItem.expandActionView();
        SearchView searchView = (SearchView) menu.findItem(R.id.search).getActionView();

        // Setting the query text to the search result activity's searchView
        searchView.setQuery(query, false);

        // This is done to avoid the keyboard popping up on the loading search result activity
        searchView.clearFocus();

        // On the search result activity, when the user modifies the search query,
        // it has to show the results on the same activity itself. Here we are capturing
        // the query text changes
        searchView.setOnQueryTextListener(queryTextListener);
    }

    // This function is just for debugging
    private void MakeToast(String text)
    {
        Context context = getApplicationContext();
        int duration = Toast.LENGTH_SHORT;
        Toast toast = Toast.makeText(context, text, duration);
        toast.show();
    }

    final SearchView.OnQueryTextListener queryTextListener = new SearchView.OnQueryTextListener() {
        @Override
        public boolean onQueryTextChange(String newText) {
            MakeToast(newText);
            return true;
        }

        @Override
        public boolean onQueryTextSubmit(String query) {
            // Do something
            return true;
        }
    };
}
